﻿using System.Linq;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using ServicePluginSelection.Models;
using ServicePluginSelection.Repositories;
using ServicePluginSelection.Services;

namespace ServicePluginSelectionTests.Services
{
    [TestClass()]
    public class ServicePluginServiceTests
    {
        [TestMethod()]
        public void SelectAllFieldsFilledOutSelectedGreaterThanPluginTest()
        {
            IServicePluginRepository pluginRepository = new ServicePluginRepository();
            IServicePluginService pluginService = new ServicePluginService(pluginRepository);

            Order order = new Order() { Country = "DE", OrganisationKey = "251", PaymentType = "CreditCard", ProductType = "Fashion", Amount = 101m, MandantId = "00001" };

            var relavantPlugins = pluginService.SelectPlugins(order);
            Assert.IsTrue(relavantPlugins.Any());

            Assert.IsTrue(relavantPlugins.First().Name == "AllFieldsFilledOutSelectedGreaterThan");
        }

        [TestMethod()]
        public void SelectAllFieldsFilledOutSelectedSmallerThanPluginTest()
        {
            IServicePluginRepository pluginRepository = new ServicePluginRepository();
            IServicePluginService pluginService = new ServicePluginService(pluginRepository);

            Order order = new Order() { Country = "DE", OrganisationKey = "251", PaymentType = "CreditCard", ProductType = "Fashion", Amount = 99m, MandantId = "00001" };

            var relavantPlugins = pluginService.SelectPlugins(order);
            Assert.IsTrue(relavantPlugins.Any());

            Assert.IsTrue(relavantPlugins.First().Name == "AllFieldsFilledOutSelectedSmallerThan");
        }

        [TestMethod()]
        public void SelectWithoutAmountPluginTest()
        {
            IServicePluginRepository pluginRepository = new ServicePluginRepository();
            IServicePluginService pluginService = new ServicePluginService(pluginRepository);

            Order order = new Order() { Country = "DE", OrganisationKey = "251", PaymentType = "CreditCard", ProductType = "Fashion", MandantId = "00001" };

            var relavantPlugins = pluginService.SelectPlugins(order);
            Assert.IsTrue(relavantPlugins.Any());

            Assert.IsTrue(relavantPlugins.First().Name == "WithoutAmount");
        }

        
    }
}