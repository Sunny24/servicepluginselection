﻿using ServicePluginSelection.Models;
using System.Collections.Generic;

namespace ServicePluginSelection.Repositories
{
    public class ServicePluginRepository : IServicePluginRepository
    {
        public ServiceConfiguration GetServiceConfiguration(string mandantId)
        {
            ServiceConfiguration serviceConfiguration = new ServiceConfiguration();
            serviceConfiguration.MandantId = mandantId;
            serviceConfiguration.ConfigurationMatrixColumns = new List<ConfigurationMatrixColumn>();

            ConfigurationMatrixColumn column1 = new ConfigurationMatrixColumn();
            column1.Plugins = new List<ServicePlugin>() { new ServicePlugin() { Name = "AllFieldsFilledOutSelectedGreaterThan" }, new ServicePlugin() { Name = "PTS" }, new ServicePlugin() { Name = "OneMore" } };
            column1.Order = 0;
            column1.OrganisationKey = "251";
            column1.Country = "DE";
            column1.PaymentType = "CreditCard";
            column1.ProductType = "Fashion";
            column1.Amount = 100m;
            column1.PriceCompareType = PriceCompareType.GreaterThan;

            ConfigurationMatrixColumn column2 = new ConfigurationMatrixColumn();
            column2.Plugins = new List<ServicePlugin>() { new ServicePlugin() { Name = "AllFieldsFilledOutSelectedSmallerThan" }, new ServicePlugin() { Name = "PTS" }, new ServicePlugin() { Name = "OneMore" } };
            column2.Order = 1;
            column2.OrganisationKey = "251";
            column2.Country = "DE";
            column2.PaymentType = "CreditCard";
            column2.ProductType = "Fashion";
            column1.Amount = 100m;
            column1.PriceCompareType = PriceCompareType.SmallerThan;

            ConfigurationMatrixColumn column3 = new ConfigurationMatrixColumn();
            column3.Plugins = new List<ServicePlugin>() { new ServicePlugin() { Name = "WithoutAmount" }, new ServicePlugin() { Name = "PTS" }, new ServicePlugin() { Name = "OneMore" } };
            column3.Order = 2;
            column3.OrganisationKey = "251";
            column3.Country = "DE";
            column3.PaymentType = "CreditCard";
            column3.ProductType = "Fashion";

            ConfigurationMatrixColumn column4 = new ConfigurationMatrixColumn();
            column4.Plugins = new List<ServicePlugin>() { new ServicePlugin() { Name = "WithoutAmountAndProductType" }, new ServicePlugin() { Name = "PTS" }, new ServicePlugin() { Name = "OneMore" } };
            column4.Order = 3;
            column4.OrganisationKey = "251";
            column4.Country = "DE";
            column4.PaymentType = "CreditCard";
            column4.ProductType = "*";

            ConfigurationMatrixColumn column5 = new ConfigurationMatrixColumn();
            column5.Plugins = new List<ServicePlugin>() { new ServicePlugin() { Name = "WithoutAmountAndProductTypeAndPaymentType" }, new ServicePlugin() { Name = "PTS" } };
            column5.Order = 4;
            column5.OrganisationKey = "251";
            column5.Country = "DE";
            column5.PaymentType = "*";
            column5.ProductType = "*";

            ConfigurationMatrixColumn column6 = new ConfigurationMatrixColumn();
            column6.Plugins = new List<ServicePlugin>() { new ServicePlugin() { Name = "Default" } };
            column6.Order = 5;
            column6.OrganisationKey = "251";
            column6.Country = "*";
            column6.PaymentType = "*";
            column6.ProductType = "*";

            serviceConfiguration.ConfigurationMatrixColumns.Add(column1);
            serviceConfiguration.ConfigurationMatrixColumns.Add(column2);
            serviceConfiguration.ConfigurationMatrixColumns.Add(column3);
            serviceConfiguration.ConfigurationMatrixColumns.Add(column4);
            serviceConfiguration.ConfigurationMatrixColumns.Add(column5);
            serviceConfiguration.ConfigurationMatrixColumns.Add(column6);

            return serviceConfiguration;
        }
    }
}
