﻿using ServicePluginSelection.Models;

namespace ServicePluginSelection.Repositories
{
    public interface IServicePluginRepository
    {
        ServiceConfiguration GetServiceConfiguration(string mandantId);
    }
}