﻿using System.Collections.Generic;
using ServicePluginSelection.Models;

namespace ServicePluginSelection.Services
{
    public interface IServicePluginService
    {
        ICollection<ServicePlugin> SelectPlugins(Order order);
    }
}