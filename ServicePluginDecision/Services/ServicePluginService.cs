﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

using ServicePluginSelection.Models;
using ServicePluginSelection.Repositories;

namespace ServicePluginSelection.Services
{
    public class ServicePluginService : IServicePluginService
    {
        private readonly IServicePluginRepository servicePluginRepository;

        public ServicePluginService(IServicePluginRepository servicePluginRepository)
        {
            this.servicePluginRepository = servicePluginRepository;
        }

        public ICollection<ServicePlugin> SelectPlugins(Order order)
        {
            ServiceConfiguration configuration = this.servicePluginRepository.GetServiceConfiguration(order.MandantId);

            var columsOrganisation = configuration.ConfigurationMatrixColumns.Where(x => x.OrganisationKey == order.OrganisationKey || x.OrganisationKey == "*").OrderBy(x => x.Order);

           var columsPrice = columsOrganisation.Where(x => this.GetPriceFilter(order.Amount, x.Amount, x.PriceCompareType)).OrderBy(x => x.Order).FirstOrDefault();


            return columsPrice.Plugins;
        }

        private bool GetPriceFilter(decimal configuredPrice, decimal orderPrice, PriceCompareType compareType)
        {
            switch (compareType)
            {
                case PriceCompareType.Equals:
                    return configuredPrice == orderPrice;
                case PriceCompareType.GreaterThan:
                    return configuredPrice >= orderPrice;
                case PriceCompareType.SmallerThan:
                    return configuredPrice < orderPrice;
                case PriceCompareType.Undefind:
                    return true;
                default:
                    return true;
            }
        }
    }
}
