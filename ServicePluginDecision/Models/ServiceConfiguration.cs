﻿using System.Collections.Generic;

namespace ServicePluginSelection.Models
{
    public class ServiceConfiguration
    {
        public string MandantId { get; set; }

        public ICollection<ConfigurationMatrixColumn> ConfigurationMatrixColumns { get; set; }
    }
}
