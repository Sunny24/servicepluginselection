﻿using System.Collections.Generic;

namespace ServicePluginSelection.Models
{
    public class ConfigurationMatrixColumn
    {
        public int Order { get; set; }

        public string OrganisationKey { get; set; }

        public string Country { get; set; }

        public decimal Amount { get; set; } = -1m;

        public PriceCompareType PriceCompareType { get; set; } = PriceCompareType.Undefind;
        public string ProductType { get; set; }

        public string PaymentType { get; set; }

        public ICollection<ServicePlugin> Plugins { get; set; }
    }
}
