﻿namespace ServicePluginSelection.Models
{
    public class Order
    {
        public string MandantId { get; set; }

        public string OrganisationKey { get; set; }

        public string Country { get; set; }

        public decimal Amount { get; set; } = -1m;

        public string ProductType { get; set; }

        public string PaymentType { get; set; }
    }
}