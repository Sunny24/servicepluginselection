﻿namespace ServicePluginSelection.Models
{
    public enum PriceCompareType
    {
        Undefind = 0,
        GreaterThan = 1,
        Equals = 2,
        SmallerThan = 3
    }
}
